<?php

namespace App\Entity;

class Cart
{
    private array $products;

    public function __construct()
    {
        $this->products = [];
    }

    public function getProducts(): array
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        //here we could sum product quantity, but in given example there are products with same id and different price, so I assume it is like marketplace that there can be different prices for product
        if($product->getQuantity() > 0)
        {
            $this->products[] = $product;
        }
        elseif ($product->getQuantity() < 0)
        {
            /** @var Product $cartProduct */
            foreach ($this->products as $key => $cartProduct)
            {
                if($cartProduct->getId() == $product->getId())
                {
                    unset($this->products[$key]);
                }
            }
        }

        return $this;
    }

    public function getTotal(): float
    {
        $total = 0;

        /** @var Product $product */
        foreach ($this->products as $product)
        {
            $total += $product->getPrice() * $product->getQuantity() / $product->getCurrency()->getRate();
        }

        return $total;
    }

}