<?php

namespace App\Entity;

class Currency
{
    private string $name;

    private float $rate;

    private bool $default;

    public function __construct()
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getDefault(): bool
    {
        return $this->rate;
    }

    public function setDefault(bool $default): self
    {
        $this->default = $default;

        return $this;
    }
}