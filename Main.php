<?php

require 'vendor/autoload.php';

    use App\Entity\Cart;
    use App\Entity\Currency;
    use App\Entity\Product;

    class Main
{
    public function main()
    {
        //load data
        $currencies = $this->addCurrencies();

        //Kai dariau, pradzioj isivaizdavau, kad dar valdysim inventoriu, turimas prekes, taciau gavus atsakymą, suprantu, kad to nereik?
        $products = $this->addProducts($currencies);

        $cart = new Cart();

        $this->addProductsToCart($cart, $products);
    }

    public function addCurrencies()
    {
        $currencies = [];

        $currencies[] = (new Currency())
            ->setName('EUR')
            ->setRate(1)
            ->setDefault(true);

        $currencies[] = (new Currency())
            ->setName('USD')
            ->setRate(1.14)
            ->setDefault(false);

        $currencies[] = (new Currency())
            ->setName('GBP')
            ->setRate(0.88)
            ->setDefault(false);

        return $currencies;
    }

    public function findCurrency(array $currencies, $currencyName)
    {
        $foundCurrency = null;
        foreach ($currencies as $currency)
        {
            if($currency->getName() == $currencyName)
            {
                $foundCurrency = $currency;
            }
        }

        //if no currency found, make it default
        if(!$foundCurrency)
        {
            foreach ($currencies as $currency)
            {
                if($currency->getDefault())
                {
                    $foundCurrency = $currency;
                }
            }
        }

        return $foundCurrency;
    }

    public function addProducts(array $currencies)
    {
        $products = [];

        $file = fopen("src/Input/products.txt","r");

        while(!feof($file))
        {
            try {
            $fileData = fgets($file);
            $productData = explode(';',$fileData);
            if(count($productData) != 5)
            {
                continue;
            }

            $product = (new Product())
                ->setId($productData[0])
                ->setName($productData[1])
                ->setQuantity((int)$productData[2])
                ->setPrice((float)$productData[3])
                ->setCurrency($this->findCurrency($currencies, str_replace(PHP_EOL, '', $productData[4])));

            if(!is_int($product->getQuantity()) || !is_float($product->getPrice()))
            {
                continue;
            }

            $products[] = $product;
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }

        fclose($file);


        return $products;
    }

    public function addProductsToCart(Cart $cart, array $products)
    {
        foreach ($products as $product) {
            $cart->addProduct($product);
            echo "Current total is: ";
            echo round($cart->getTotal(),2);
            echo "\n";
        }
    }
}

$main = new Main();

?>